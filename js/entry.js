var music = new Audio('./audios/badapple.mp3');

var timer = null;
var frame = 0;
var play = document.getElementById('play');
var pause = document.getElementById('pause');
var ani = document.getElementById('ani');
var ani2 = document.getElementById('ani2');
var loading = document.getElementById('loading');

play.onclick = function () {
  pause.removeAttribute('disabled');
  play.setAttribute('disabled', true);
  music.play();
  timer = setInterval (function () {
    if (frame === data.length) {
      frame = 0;
    }
    var color1 = Math.round(Math.random() * 16);
    var color2 = Math.round(Math.random() * 16);
    var color3 = Math.round(Math.random() * 16);
    var color = '#' + color1.toString(16) + color2.toString(16) + color3.toString(16) ; 
    ani.innerHTML = data[frame];
    ani2.innerHTML = data[frame];
    ani.style.color = color;
    frame ++;
  }, 33.3333);
}

pause.onclick = function () {
  pause.setAttribute('disabled', true);
  play.removeAttribute('disabled');
  music.pause();
  clearInterval (timer);
};

music.onended = function () {
  music.pause();
  clearInterval (timer);
  ani.innerHTML = '';
  ani2.innerHTML = '';
  loading.className = '';
  loading.style.display = 'block';
};

document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
    loading.className = 'fade-out';
    setTimeout(function () {
      loading.style.display = 'none';
    }, 300);
  }
};